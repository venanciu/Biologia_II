# Histolgia

**Histo** (tecido) + **logia** (estudo)
= **Estudo** dos **tecidos**

### Tecido
Conjuto de celulas
que se reunem
para uma determinada ação.
São formados por **celulas**
e **matriz extracelular**

Grupos Principais:
- Epitelial
  - Revestem o corpo externamente e internamente e formam glândulas
- Conjutivo
  - Preenchem e sustentam
- Muscular
  - Resposáveis por movimento
- Nervoso
  - Formado por celulas nervosas

### Orgão

Conjuto de tecidos
para realizar uma ou mais funções

### Matriz extracelular

Substancia que junta as celulas.
São feitas de:
- colageno, proteoglicanos
  - Para dar resistencia
- glicoproteinas
  - Para dar fiscosidade
- integrinas
  - Possibilita comunicação entre outras celulas e a propria matriz

Funções:
- preencher espacos
- resistencia aos tecidos
- veiculo de transporte de substancias
- veiculo de migracao das celulas


# Tecido Epitelial

Um dos 4 grandes grupos
que temos no nosso corpo

#### Funções:
- Revestimento
- Absorcao de moleculas (Ex. intestino)
- Secreção (Ex. Gladulas sudoriperas, lacrimais)

#### Características:
- Dispostas justaposmente
  * consequentemente
há poucas substancias
entre as células,
por exemplo matriz extracelular.
- Alta taxa de proliferação, mitótica
- Avasculação
  * Não tem sangue.
Recebendo oxigênio e nutrientes
através do tecido conjutivo abaixo
chegados através de difusão.

#### Classificações com base na sua forma
- Pavimentosas (Achatadas, lembrando um pavimento)
- Cúbicas
- Colonais ou prismáticas

#### Classificação de acordo com quantidade de camadas
- Simples
  - Única camada
- Extratificados
  - Várias camadas
- Pseudo-estratificados
  - Aparenta ser extratificado por ter alturas diferentes mas possui apenas uma camada

#### Epitelios Glandulares
Produzem e secretam substancias que podem ser
- Endócrinas
  - produzem hormônios que serão liberados na nossa corrente sanguínea
- Exócrinas
  - produzem substancias que serão jogadas para fora do corpo ou para cavidades corporais


# Tecido Glandular

Formadas por tecido epitelial

Características
- Celulas justapostas
- Avascularizado
- Alta taxa de proliferação

Função:
- Secreção

Grupos:
- Glândulas exócrinas
    - possuem ductos de secreção
- Glândulas endócrinas
    - seus hormônios são liberados na corrente sanguínea
- Glandulas mistas ou anfícrinas
    - São exócrinas e endócinas ao mesmo tempo
    - Ex. pâncreas

Classificação
- Merócrina
  - Libera somente a substancia que produziu
- Holócrina (Ex. Sebácias)
  - Libera suas células junto da substancia
- Apócrina (Ex. Mamárias)
  - Fragmentos da células liberados junto

Formação
1. Invaginação:
  - Celulas superficiais crescem para dentro, em direção ao tecido mais profundo
2. Surgimento da área secretora
  - Celulas mais profundas se trasformarão na **área secretora**
3. Celulas de conexão
  - Conecta área secretora à parte superficial
4.1. (Exocrina) Formação do ducto secretor
  - Redireciona as substancias ate o meio externo
4.2. (Endocrina) Isolamento da parte secretora com a superfície
  - Celulas de conexao são dissolvidas no organismo
  - Substancias liberadas diretamentes no capilares sanguineos ao redor


# Tecido Conjutivo

Funções
- União entre tecidos ou estruturas
- Preenchimento
- Sustentação

Todo tecido conjutivo
possuí mesma origem embrionária
(mesoderma)

Possui
- Celulas
- Matriz extracelular

As variações
de celulas
e de matrizes extracelulares
possibilatam
a grande variadade de 
tecidos conjutivos:

- Tecido conjutivo propriamente dito
- Tecido ósseo
- Tecido adiposo
- Tecido cartilaginoso
- Tecido hematopoiético

Composição da matriz extracelular:
- Substancia Fundamental (Em abundancia)
  - Água
  - Proteinas
  - Lipídeos
  - Carboidratos
- Fibras:
  - Elásticas (Ex. derme)
      - Resposáveis por elasticidade
  - Colágenas
      - Resposáveis por resistencia
  - Reticulares
      - Sustentação, auxiliar o sistema imunológico

### Tecido conjutivo propriamente dito

Células
- Fibroblastos (Ou fibrocito)
  - Produzem fibras
  - Chama fibroblastos se estiver em alta atividade
  - Chama fibrocito se estiver em baixa atividade
- Adipócitos
  - Armazenam gordura
- Mastócitos
  - Produzem estamina, eparina (anti-coagulante)
- Mesequimais
  - Não possuem característica própria
  - Mas conseguem se diferenciar de outras células
- Macrófagos
  - Fagocitose
- Leocócitos (Grupo de células
  - Celulas sanguíneas que ajudam no sistema imunológico

Classificações
- Frouxo
  - Preenchimento
- Denso
  - Não modelado
      - Fibras colagenas não organizadadas
      - Susteta outros tecidos
  - Modelado
      - Fibras colagenas organizadas
      - Mais resistente

### Tecido ósseo

Características
da matriz extracelular
do tecido ósseo
- Rígida, calcificada
- Parte inorgânica (matriz mineral)
  - Representa 65% da matriz
  - Formada por cálcio, fósforo, sódio, potássio, magnésio, bicarbonato
  - Principais compontes: fósforo e cálcio (fosfato de cálcio)
  - Fosfato de cálcio se cristaliza formando cristais de hidroxiapatita
- Parte orgânica
  - Fibras de colágeno, glicoproteinas, proteoglicanos

Celulas
da matriz extracelular
do tecido ósseo
- Osteoblastos
  - Cosntante atividade
  - Sintetizam a parte orgânica
  - Resposáveis pelo crescimento ou renovação
  - Constroem o tecido
- Osteócitos
  - Osteoblastos amadurecidos, menor atividade
  - Ficam dentro de lacunas da matrix extracelular
- Osteoclastos
  - Gigantes
  - Reabsorvem tecido ósseo
  - Reformam o tecido
- Osteogêncas
  - Celulas indiferenciadas que consguem se diferenciar no osteoblastos se necessário

